**Project 1: Expense Reimbursement System**

**Project Description:**
The Expense Reimbursement System (ERS) allows employees to login and submit requests for reimbursements and view their past and pending requests. Pending requests can be approved or denied by a manager.

**Technologies Used:** Java, HTML, JavaScript, SQL, Apache Tomcat, Git, Maven, Bootstrap

**Features Implemented:**
 - Login as employee or manager
 - Employee: Submit reimbursement request
 - Employee: View list of past requests
 - Manager: View list of pending requests
 - Manager: Approve or deny pending requests

**Getting Started:**
 1. Open Git Bash or similar application and run the command following command: git clone https://gitlab.com/Austen-Seidler/project-1.git
 2. Open cloned project in Eclipse or a similar IDE
 3. Ensure that Apache Tomcat 9.0 is the targeted runtime in the project's properties, then Run on Server
 4. Open http://localhost:8080/ProjectOne/html/index.html in a browser window

**Usage:**
 1. Input a valid username and password to login. 
    - Employee Example: username=demouser, password=password
    - Manager Example: username=demomanager, password=admin
 2. To view personal info, select View Info.
 3. As an employee, select View Reimbursement History to see past reimbursements the logged-in employee has submitted.
 4. As an employee, select Submit Reimbursement Request to bring up thhe submission page, then input required info and select Submit to submit the request
 5. As a manager, select View Pending Requests to see pending requests, then select either Approve Request or Deny Request and select a request from the given menu to approve or deny a reimbursement request.
 6. To log out, select Log Out
