/*
*
*/

window.onload = function() {
	displayReimbursements();
}

function displayReimbursements() {
	let xhttp = new XMLHttpRequest();
	console.log("before open");
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status==200){
			let reimbursements = JSON.parse(xhttp.responseText);
			console.log(reimbursements);
			for (let temp of reimbursements) {
				addReimbursement(temp);
			}
		}

	}
	
	xhttp.open("GET", "http://localhost:8080/ProjectOne/getsessionreimbursements.json");
	console.log("after open");
	
	xhttp.send();
}

//The following function created with the help of stackoverflow
//https://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
function addReimbursement(temp) {
  const div = document.createElement('div');

  div.className = 'col';
  var date = new Date(temp.submitted);
  if (temp.resolver != "" && temp.resolver != null) {
	var date2 = new Date(temp.resolved);
	div.innerHTML = `
	    <div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Reimbursement ID: ${temp.reimburseId}</h5>
		    <p class="card-text">Amount: $${temp.amount}</p>
			<p class="card-text">Description: ${temp.description}</p>
			<p class="card-text">Submitted on: ${date.getMonth()+1}-${date.getDate()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}</p>
			<p class="card-text">Reviewed by: ${temp.resolver}</p>
			<p class="card-text">Reviewed on: ${date2.getMonth()+1}-${date2.getDate()}-${date2.getFullYear()} ${date2.getHours()}:${date2.getMinutes()}:${date2.getSeconds()}</p>
			<p class="card-text">Status: ${temp.status}</p>
			<p class="card-text">Type: ${temp.type}</p>
		  </div>
		</div>
	  `;
  }
  else {
	div.innerHTML = `
	    <div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Reimbursement ID: ${temp.reimburseId}</h5>
		    <p class="card-text">Amount: $${temp.amount}</p>
			<p class="card-text">Description: ${temp.description}</p>
			<p class="card-text">Submitted on: ${date.getMonth()+1}-${date.getDate()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}</p>
			<p class="card-text">Status: ${temp.status}</p>
			<p class="card-text">Type: ${temp.type}</p>
		  </div>
		</div>
	  `;
  }

  document.getElementById('reimbursements').appendChild(div);
}
