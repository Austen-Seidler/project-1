/*
*
*/

window.onload = function() {
	console.log("hello from js");
	getSessUser();
}

function getSessUser() {
	let xhttp = new XMLHttpRequest();
	console.log("before open");
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status==200){
			let user = JSON.parse(xhttp.responseText);
			console.log(user);
			document.getElementById("employeeWelcome").innerText=`Welcome ${user.username}`;
		}

	}
	
	xhttp.open("GET", "http://localhost:8080/ProjectOne/getsessionuser.json");
	console.log("after open");
	
	xhttp.send();
}