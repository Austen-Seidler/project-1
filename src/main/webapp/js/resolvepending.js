/*
*
*/

window.onload = function() {
	displayReimbursements();
}

function displayReimbursements() {
	let xhttp = new XMLHttpRequest();
	console.log("before open");
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status==200){
			let reimbursements = JSON.parse(xhttp.responseText);
			console.log(reimbursements);
			var i = 0;
			for (let temp of reimbursements) {
				addReimbursement(temp, i);
				i++;
			}
		}

	}
	
	xhttp.open("GET", "http://localhost:8080/ProjectOne/getsessionreimbursements.json");
	console.log("after open");
	
	xhttp.send();
}

//The following function created with the help of stackoverflow
//https://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
function addReimbursement(temp, i) {
  const option = document.createElement('option');

  if (i == 0) {
	option.selected == true;
  }

  option.value == temp.reimburseId;


  option.innerHTML = `
    ${temp.reimburseId}
  `;

  document.getElementById('reimbursements').appendChild(option);
}
