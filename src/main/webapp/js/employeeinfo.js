/*
*
*/

window.onload = function() {
	displayInfo();
}

function displayInfo() {
	let xhttp = new XMLHttpRequest();
	console.log("before open");
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status==200){
			let user = JSON.parse(xhttp.responseText);
			console.log(user);
			document.getElementById("username").innerText=`${user.username}`;
			document.getElementById("name").innerText=`Name: ${user.firstName} ${user.lastName}`;
			document.getElementById("email").innerText=`Email: ${user.email}`;
			document.getElementById("user_id").innerText=`User ID: ${user.userId}`;
		}

	}
	
	xhttp.open("GET", "http://localhost:8080/ProjectOne/getsessionuser.json");
	console.log("after open");
	
	xhttp.send();
}