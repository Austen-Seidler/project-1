package com.projectone.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectone.MainDriver;
import com.projectone.model.Reimbursement;
import com.projectone.model.User;
import com.projectone.service.ERSService;

public class ERSController {
	
	public String login(HttpServletRequest req, ERSService serv) {
		//System.out.println("in ers controller login");
		if(!req.getMethod().equals("POST")) {
			return "html/error.html";
		}
		
		User user = serv.getUserFromCredentials(req.getParameter("username"), req.getParameter("password"));
		//System.out.println(req.getParameter("username"));
		//System.out.println(req.getParameter("password"));
		//System.out.println(serv.getUserFromCredentials(req.getParameter("username"), req.getParameter("password")));
		
		if (user == null) {
			//System.out.println("user null");
			return "wrongcreds.view";
		}
		else if (user.getRole().equals("Manager") /*maybe change this later to use service/lDao*/) {
			MainDriver.log.info("Login: " + user.getUsername());
			req.getSession().setAttribute("currentUser", user);
			return "html/managerhome.html";
		}
		else {
			MainDriver.log.info("Login: " + user.getUsername());
			req.getSession().setAttribute("currentUser", user);
			return "html/employeehome.html";
		}
	}
	
	public String returnHome(HttpServletRequest req, ERSService serv) {
		User user = (User)req.getSession().getAttribute("currentUser");
		if (user == null) {
			//System.out.println("user null");
			return "wrongcreds.view";
		}
		else if (user.getRole().equals("Manager") /*maybe change this later to use service/lDao*/) {
			//req.getSession().setAttribute("currentUser", user);
			return "html/managerhome.html";
		}
		else {
			//req.getSession().setAttribute("currentUser", user);
			return "html/employeehome.html";
		}
		
	}
	
	public String getReimbursements(HttpServletRequest req, ERSService serv) {
		User user = (User)req.getSession().getAttribute("currentUser");
		List<Reimbursement> reimbursements = serv.getUserReimbursements(user);
		if (reimbursements == null || reimbursements.size() == 0) {
			//System.out.println("no reimbursements");
			return "html/noreimbursements.html";
		}
		else {
			//System.out.println("in getreimbursements");
			req.getSession().setAttribute("reimbursements", reimbursements);
			return "html/viewreimbursements.html";
		}
		
	}
	
	public String submitReimbursement(HttpServletRequest req, ERSService serv) {
		//System.out.println("in ers controller submit reimbursement");
		User user = (User)req.getSession().getAttribute("currentUser");
		if(!req.getMethod().equals("POST")) {
			return "html/error.html";
		}
		
		//System.out.println(req.getParameter("type"));
		String parameter;
		if (req.getParameter("type").equals("1")) {
			parameter = "Travel";
		}
		else if (req.getParameter("type").equals("2")) {
			parameter = "Food";
		}
		else if (req.getParameter("type").equals("3")) {
			parameter = "Other";
		}
		else if (req.getParameter("type").equals("0")) {
			parameter = "Lodging";
		}
		else {
			parameter = req.getParameter("type");
		}
		if (serv.addReimbursement(req.getParameter("amount"), req.getParameter("description"), user.getUsername(), parameter)) {
			MainDriver.log.info(user.getUsername() + " submitted reimbursement for $" + req.getParameter("amount"));
			return "html/success.html";
		}
		else {
			//System.out.println("error in submission");
			MainDriver.log.error("Error submitting reimbursement");
			return "error.view";
		}
		
	}
	
	public String getPendingReimbursements(HttpServletRequest req, ERSService serv) {
		//System.out.println("in ers controller get pending reimbursements");
		List<Reimbursement> reimbursements = serv.getPendingReimbursements();
		if (reimbursements == null || reimbursements.size() == 0) {
			//System.out.println("no reimbursements");
			return "html/noreimbursements.html";
		}
		else {
			//System.out.println("in getreimbursements");
			req.getSession().setAttribute("reimbursements", reimbursements);
			return "html/viewpending.html";
		}
	}
	
	public String approveReimbursement(HttpServletRequest req, ERSService serv) {
		//System.out.println("in ers controller approve reimbursement");
		User user = (User)req.getSession().getAttribute("currentUser");
		//System.out.println(req.getParameter("reimbursement"));
		serv.approveReimbursement(Integer.parseInt(req.getParameter("reimbursement")), user.getUsername());
		MainDriver.log.info(user.getUsername() + " approved reimbursement #" + req.getParameter("reimbursement"));
		return "html/approved.html";
	}
	
	public String denyReimbursement(HttpServletRequest req, ERSService serv) {
		//System.out.println("in ers controller deny reimbursement");
		User user = (User)req.getSession().getAttribute("currentUser");
		serv.denyReimbursement(Integer.parseInt(req.getParameter("reimbursement")), user.getUsername());
		MainDriver.log.info(user.getUsername() + " denied reimbursement #" + req.getParameter("reimbursement"));
		return "html/denied.html";
	}
	
	public void getSessionUser(HttpServletRequest req, HttpServletResponse res, ERSService serv) throws JsonProcessingException, IOException {
		User user = (User)req.getSession().getAttribute("currentUser");
		res.getWriter().write(new ObjectMapper().writeValueAsString(user));
		//System.out.println(new ObjectMapper().writeValueAsString(user));
	}
	
	public void getSessionReimbursements(HttpServletRequest req, HttpServletResponse res, ERSService serv) throws JsonProcessingException, IOException {
		List<Reimbursement> reimbursements = (List<Reimbursement>)req.getSession().getAttribute("reimbursements");
		res.getWriter().write(new ObjectMapper().writeValueAsString(reimbursements));
		//System.out.println(new ObjectMapper().writeValueAsString(reimbursements));
	}
	
}
