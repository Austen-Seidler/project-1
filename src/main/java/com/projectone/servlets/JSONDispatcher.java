package com.projectone.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectone.controller.ERSController;
import com.projectone.model.User;
import com.projectone.service.ERSService;

public class JSONDispatcher {
	
	static ERSController control = new ERSController();
	static ERSService serv = new ERSService();
	
	public static void process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		switch(req.getRequestURI()) {
		
		case "/ProjectOne/getsessionuser.json":
			System.out.println("in getsessionuser.json dispatcher");
			control.getSessionUser(req, res, serv);
			break;
			
		case "/ProjectOne/getsessionreimbursements.json":
			System.out.println("in getsessionreimbursements.json dispatcher");
			control.getSessionReimbursements(req, res, serv);
			break;
			
		default:
			System.out.println("in json default");
			res.getWriter().write(new ObjectMapper().writeValueAsString(new User()));
		
		}
		
	}
	
}
