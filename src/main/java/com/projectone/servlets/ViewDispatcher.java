package com.projectone.servlets;

import javax.servlet.http.HttpServletRequest;

import com.projectone.controller.ERSController;
import com.projectone.service.ERSService;

public class ViewDispatcher {
	
	static ERSController control = new ERSController();
	static ERSService serv = new ERSService();
	
	public static String process(HttpServletRequest req) {
		
		switch(req.getRequestURI()) {
		
		case "/ProjectOne/login.view":
			System.out.println("in login.view dispatcher");
			return control.login(req, serv);
			
		case "/ProjectOne/wrongcreds.view":
			System.out.println("in wrongcreds.view dispatcher");
			return "html/failedlogin.html";
			
		case "/ProjectOne/home.view":
			System.out.println("in home.view dispatcher");
			return control.returnHome(req, serv);
			
		case "/ProjectOne/history.view":
			System.out.println("in history.view dispatcher");
			return control.getReimbursements(req, serv);
			
		case "/ProjectOne/submit.view":
			System.out.println("in submit.view dispatcher");
			return control.submitReimbursement(req, serv);
			
		case "/ProjectOne/pending.view":
			System.out.println("in pending.view dispatcher");
			return control.getPendingReimbursements(req, serv);
			
		case "/ProjectOne/approve.view":
			System.out.println("in approve.view dispatcher");
			return control.approveReimbursement(req, serv);
			
		case "/ProjectOne/deny.view":
			System.out.println("in deny.view dispatcher");
			return control.denyReimbursement(req, serv);
			
		default:
		 	System.out.println("in view default");
		 	return "html/error.html";
		
		}
		
	}
	
}
