package com.projectone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.projectone.MainDriver;

public class LookupDaoImpl {
	
	private DBConnection dbCon;
	
	public LookupDaoImpl() {
		
	}

	public LookupDaoImpl(DBConnection dbCon) {
		super();
		this.dbCon = dbCon;
	}
	
	public String getUserRole(int id) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select u_role from user_roles where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return "";
		
	}
	
	public int getUserRoleId(String role) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select id from user_roles where u_role=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, role);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
	public String getUser(int id) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select username from users where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return "";
		
	}
	
	public int getUserId(String username) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select id from users where username=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
	public String getReimbursementStatus(int id) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select r_status from reimbursement_statuses where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return "";
		
	}
	
	public int getReimbursementStatusId(String role) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select id from reimbursement_statuses where r_status=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, role);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
	public String getReimbursementType(int id) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select r_type from reimbursement_types where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return "";
		
	}
	
	public int getReimbursementTypeId(String role) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select id from reimbursement_types where r_type=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, role);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
}
