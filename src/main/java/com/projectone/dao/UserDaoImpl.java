package com.projectone.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.projectone.MainDriver;
import com.projectone.model.User;

public class UserDaoImpl {
	
	private DBConnection dbCon;
	private LookupDaoImpl lDao;
	
	public UserDaoImpl() {
		
	}
	
	public UserDaoImpl(DBConnection dbCon, LookupDaoImpl lDao) {
		super();
		this.dbCon = dbCon;
		this.lDao = lDao;
	}

	public List<User> getAll() {
		
		List<User> userList = new ArrayList<>();
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from users";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				userList.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), lDao.getUserRole(rs.getInt(7))));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return userList;
		
	}
	
	public User getById(int id) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from users where id=?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			User user = new User();
			while(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), lDao.getUserRole(rs.getInt(7)));
			}
			return user;
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return null;
		
	}
	
	public User getByName(String name) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from users where username=?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			User user = new User();
			while(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), lDao.getUserRole(rs.getInt(7)));
			}
			return user;
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return null;
		
	}
	
	//updating by id only requires one user object (since the id will still be the same)
	public void update(User user) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "update users set username=?, u_password=?, first_name=?, last_name=?, email=?, r_id=? where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFirstName());
			ps.setString(4, user.getLastName());
			ps.setString(5, user.getEmail());
			ps.setInt(6, lDao.getUserRoleId(user.getRole()));
			ps.setInt(7, user.getUserId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
	}
	
	//updating by username requires the old user object and the new one (since username can change)
	public void update(User oldUser, User newUser) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "update users set username=?, u_password=?, first_name=?, last_name=?, email=?, r_id=? where username=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newUser.getUsername());
			ps.setString(2, newUser.getPassword());
			ps.setString(3, newUser.getFirstName());
			ps.setString(4, newUser.getLastName());
			ps.setString(5, newUser.getEmail());
			ps.setInt(6, lDao.getUserRoleId(newUser.getRole()));
			ps.setString(7, oldUser.getUsername());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
	}
	
	public int insert(User user) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "{? = call insert_user(?,?,?,?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setString(2, user.getUsername());
			cs.setString(3, user.getPassword());
			cs.setString(4, user.getFirstName());
			cs.setString(5, user.getLastName());
			cs.setString(6, user.getEmail());
			cs.setInt(7, lDao.getUserRoleId(user.getRole()));
			cs.execute();
			return cs.getInt(1);
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
	public void delete(User user) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "delete from users where username=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
	}
	
}
