package com.projectone.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.projectone.MainDriver;
import com.projectone.model.Reimbursement;

public class ReimbursementDaoImpl {
	
	private DBConnection dbCon;
	private LookupDaoImpl lDao;
	
	public ReimbursementDaoImpl() {
		
	}
	
	public ReimbursementDaoImpl(DBConnection dbCon, LookupDaoImpl lDao) {
		super();
		this.dbCon = dbCon;
		this.lDao = lDao;
	}

	public List<Reimbursement> getAll() {
		
		List<Reimbursement> reimburseList = new ArrayList<>();
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from reimbursements";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				reimburseList.add(new Reimbursement(rs.getInt(1), rs.getBigDecimal(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						lDao.getUser(rs.getInt(6)), lDao.getUser(rs.getInt(7)), lDao.getReimbursementStatus(rs.getInt(8)), lDao.getReimbursementType(rs.getInt(9))));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return reimburseList;
		
	}
	
	public List<Reimbursement> getAllByAuthorId(int id) {
		
		List<Reimbursement> reimburseList = new ArrayList<>();
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from reimbursements where author_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				reimburseList.add(new Reimbursement(rs.getInt(1), rs.getBigDecimal(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						lDao.getUser(rs.getInt(6)), lDao.getUser(rs.getInt(7)), lDao.getReimbursementStatus(rs.getInt(8)), lDao.getReimbursementType(rs.getInt(9))));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return reimburseList;
		
	}
	
	public List<Reimbursement> getAllPending() {
		
		List<Reimbursement> reimburseList = new ArrayList<>();
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from reimbursements where status_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, lDao.getReimbursementStatusId("Pending"));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				reimburseList.add(new Reimbursement(rs.getInt(1), rs.getBigDecimal(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						lDao.getUser(rs.getInt(6)), lDao.getUser(rs.getInt(7)), lDao.getReimbursementStatus(rs.getInt(8)), lDao.getReimbursementType(rs.getInt(9))));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return reimburseList;
		
	}
	
	public Reimbursement getById(int id) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "select * from reimbursements where id=?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Reimbursement reimburse = new Reimbursement();
			while (rs.next()) {
				reimburse = new Reimbursement(rs.getInt(1), rs.getBigDecimal(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						lDao.getUser(rs.getInt(6)), lDao.getUser(rs.getInt(7)), lDao.getReimbursementStatus(rs.getInt(8)), lDao.getReimbursementType(rs.getInt(9)));
			}
			return reimburse;
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return null;
		
	}
	
	public void update(Reimbursement reimburse) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "update reimbursements set amount=?, submitted=?, resolved=?, description=?, author_id=?, resolver_id=?, status_id=?, type_id=? where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setBigDecimal(1, reimburse.getAmount());
			ps.setTimestamp(2, reimburse.getSubmitted());
			ps.setTimestamp(3, reimburse.getResolved());
			ps.setString(4, reimburse.getDescription());
			ps.setInt(5, lDao.getUserId(reimburse.getAuthor()));
			ps.setInt(6, lDao.getUserId(reimburse.getResolver()));
			ps.setInt(7, lDao.getReimbursementStatusId(reimburse.getStatus()));
			ps.setInt(8, lDao.getReimbursementTypeId(reimburse.getType()));
			ps.setInt(9, reimburse.getReimburseId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
	}
	
	public int insert(Reimbursement reimburse) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "{? = call insert_reimbursement(?,?,?,?,?,?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setBigDecimal(2, reimburse.getAmount());
			cs.setTimestamp(3, reimburse.getSubmitted());
			cs.setTimestamp(4, reimburse.getResolved());
			cs.setString(5, reimburse.getDescription());
			cs.setInt(5, lDao.getUserId(reimburse.getAuthor()));
			cs.setInt(6, lDao.getUserId(reimburse.getResolver()));
			cs.setInt(7, lDao.getReimbursementStatusId(reimburse.getStatus()));
			cs.setInt(8, lDao.getReimbursementTypeId(reimburse.getType()));
			cs.execute();
			return cs.getInt(1);
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
	public int insertUnresolved(Reimbursement reimburse) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "{? = call insert_reimbursement_unresolved(?,?,?,?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setBigDecimal(2, reimburse.getAmount());
			cs.setTimestamp(3, reimburse.getSubmitted());
			cs.setString(4, reimburse.getDescription());
			cs.setInt(5, lDao.getUserId(reimburse.getAuthor()));
			cs.setInt(6, lDao.getReimbursementStatusId(reimburse.getStatus()));
			cs.setInt(7, lDao.getReimbursementTypeId(reimburse.getType()));
			cs.execute();
			return cs.getInt(1);
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
		return 0;
		
	}
	
	public void delete(Reimbursement reimburse) {
		
		try (Connection con = dbCon.getDBConnection()) {
			
			String sql = "delete from reimbursements where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimburse.getReimburseId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			MainDriver.log.error(e.getMessage());
		}
		
	}
	
}
