package com.projectone.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Reimbursement {
	
	private int reimburseId;
	private BigDecimal amount;
	private Timestamp submitted;
	private Timestamp resolved;
	private String description;
	private String author;
	private String resolver;
	private String status;
	private String type;
	
	public Reimbursement() {
		
	}

	public Reimbursement(int reimburseId, BigDecimal amount, Timestamp submitted, Timestamp resolved,
			String description, String author, String resolver, String status, String type) {
		super();
		this.reimburseId = reimburseId;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}
	
	public Reimbursement(BigDecimal amount, Timestamp submitted, Timestamp resolved,
			String description, String author, String resolver, String status, String type) {
		super();
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}
	
	public Reimbursement(int reimburseId, double amount, Timestamp submitted, Timestamp resolved,
			String description, String author, String resolver, String status, String type) {
		super();
		this.reimburseId = reimburseId;
		this.amount = BigDecimal.valueOf(amount);
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}
	
	public Reimbursement(double amount, Timestamp submitted, Timestamp resolved,
			String description, String author, String resolver, String status, String type) {
		super();
		this.amount = BigDecimal.valueOf(amount);
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}

	public int getReimburseId() {
		return reimburseId;
	}

	public void setReimburseId(int reimburseId) {
		this.reimburseId = reimburseId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}

	public Timestamp getResolved() {
		return resolved;
	}

	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getResolver() {
		return resolver;
	}

	public void setResolver(String resolver) {
		this.resolver = resolver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Reimbursement [reimburseId=" + reimburseId + ", amount=" + amount + ", submitted=" + submitted
				+ ", resolved=" + resolved + ", description=" + description + ", author=" + author + ", resolver="
				+ resolver + ", status=" + status + ", type=" + type + "]";
	}
	
}
