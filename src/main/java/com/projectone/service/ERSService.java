package com.projectone.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.projectone.dao.DBConnection;
import com.projectone.dao.LookupDaoImpl;
import com.projectone.dao.ReimbursementDaoImpl;
import com.projectone.dao.UserDaoImpl;
import com.projectone.model.Reimbursement;
import com.projectone.model.User;

public class ERSService {
	
	private DBConnection con;
	private UserDaoImpl uDao;
	private ReimbursementDaoImpl rDao;
	private LookupDaoImpl lDao;
	
	public ERSService() {
		super();
		con = new DBConnection();
		lDao = new LookupDaoImpl(con);
		uDao = new UserDaoImpl(con, lDao);
		rDao = new ReimbursementDaoImpl(con, lDao);
	}
	
	public User getUserFromCredentials(String username, String password) {
		
		User user = uDao.getByName(username);
		if (user.getPassword() != null) {
			if (user.getPassword().equals(password)) {
				return user;
			}
		}
		
		return null;
		
	}
	
	public List<Reimbursement> getUserReimbursements(User user) {
		
		if (user != null) {
			return rDao.getAllByAuthorId(user.getUserId());
		}
		
		return null;
		
	}
	
	public boolean addReimbursement(String amount, String description, String author, String type) {
		
		if (amount != null && description != null && author != null && type != null) {
			Reimbursement reimburse = new Reimbursement();
			reimburse.setAmount(BigDecimal.valueOf(Double.parseDouble(amount)));
			reimburse.setSubmitted(new Timestamp(System.currentTimeMillis()));
			reimburse.setDescription(description);
			reimburse.setAuthor(author);
			reimburse.setStatus("Pending");
			reimburse.setType(type);
			if (rDao.insertUnresolved(reimburse) != 0) {
				return true;
			}
		}
		
		return false;
		
	}
	
	public List<Reimbursement> getPendingReimbursements() {
		
		return rDao.getAllPending();
		
	}
	
	public void approveReimbursement(int id, String resolver) {
		
		Reimbursement reimburse = rDao.getById(id);
		reimburse.setResolved(new Timestamp(System.currentTimeMillis()));
		reimburse.setResolver(resolver);
		reimburse.setStatus("Approved");
		rDao.update(reimburse);
		
	}
	
	public void denyReimbursement(int id, String resolver) {
		
		Reimbursement reimburse = rDao.getById(id);
		reimburse.setResolved(new Timestamp(System.currentTimeMillis()));
		reimburse.setResolver(resolver);
		reimburse.setStatus("Denied");
		rDao.update(reimburse);
		
	}
	
}
