package com.projectone.eval.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.projectone.controller.ERSController;
import com.projectone.model.Reimbursement;
import com.projectone.model.User;
import com.projectone.service.ERSService;

public class ERSControllerTest {
	
	@Mock
	private HttpServletRequest req;
	@Mock
	private HttpSession sess;
	@Mock
	private ERSService serv;
	private ERSController control;
	private User user, manager;
	private List<Reimbursement> reimbursements;
	
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		control = new ERSController();
		user = new User("testuser", "password", "test", "user", "email@email.com", "Employee");
		manager = new User("manager", "mpassword", "mr", "manageman", "manager@email.com", "Manager");
		reimbursements = new ArrayList<>();
		reimbursements.add(new Reimbursement(50.00, new Timestamp(System.currentTimeMillis()), null, "test", "testuser", null, "Pending", "Travel"));
		reimbursements.add(new Reimbursement(101.12, new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()), "test", "testuser", "manager", "Approved", "Lodging"));
		when(req.getMethod()).thenReturn("POST");
		when(req.getSession()).thenReturn(sess);
	}
	
	@Test
	public void testLoginAsEmployee() {
		when(req.getParameter("username")).thenReturn("testuser");
		when(req.getParameter("password")).thenReturn("password");
		when(serv.getUserFromCredentials("testuser", "password")).thenReturn(user);
		assertEquals(control.login(req, serv), "html/employeehome.html");
	}
	
	@Test
	public void testLoginAsManager() {
		when(req.getParameter("username")).thenReturn("manager");
		when(req.getParameter("password")).thenReturn("mpassword");
		when(serv.getUserFromCredentials("manager", "mpassword")).thenReturn(manager);
		assertEquals(control.login(req, serv), "html/managerhome.html");
	}
	
	@Test
	public void testLoginFail() {
		when(req.getParameter("username")).thenReturn("");
		when(req.getParameter("password")).thenReturn("");
		when(serv.getUserFromCredentials("", "")).thenReturn(null);
		assertEquals(control.login(req, serv), "wrongcreds.view");
	}
	
	@Test
	public void testGetReimbursements() {
		when((User)req.getSession().getAttribute("currentUser")).thenReturn(user);
		when(serv.getUserReimbursements(user)).thenReturn(reimbursements);
		assertEquals(control.getReimbursements(req, serv), "html/viewreimbursements.html");
	}
	
	@Test
	public void testGetReimbursementsFailure() {
		when((User)req.getSession().getAttribute("currentUser")).thenReturn(user);
		when(serv.getUserReimbursements(user)).thenReturn(null);
		assertEquals(control.getReimbursements(req, serv), "html/noreimbursements.html");
	}
	
	@Test
	public void testSubmitReimbursement() {
		when((User)req.getSession().getAttribute("currentUser")).thenReturn(user);
		when(req.getParameter("type")).thenReturn("Lodging");
		when(req.getParameter("amount")).thenReturn("200.56");
		when(req.getParameter("description")).thenReturn("test");
		when(serv.addReimbursement("200.56", "test", "testuser", "Lodging")).thenReturn(true);
		assertEquals(control.submitReimbursement(req, serv), "html/success.html");
	}
	
	@Test
	public void testSubmitReimbursementFailure() {
		when((User)req.getSession().getAttribute("currentUser")).thenReturn(user);
		when(req.getParameter("type")).thenReturn("");
		when(req.getParameter("amount")).thenReturn("");
		when(req.getParameter("description")).thenReturn("");
		when(serv.addReimbursement("", "", "testuser", "")).thenReturn(false);
		assertEquals(control.submitReimbursement(req, serv), "error.view");
	}
	
	@Test
	public void testGetPendingReimbursements() {
		when(serv.getPendingReimbursements()).thenReturn(reimbursements);
		assertEquals(control.getPendingReimbursements(req, serv), "html/viewpending.html");
	}
	
	@Test
	public void testGetPendingReimbursementsFailure() {
		when(serv.getPendingReimbursements()).thenReturn(null);
		assertEquals(control.getPendingReimbursements(req, serv), "html/noreimbursements.html");
	}
	
}
